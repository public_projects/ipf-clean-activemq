package org.green.ipf.clean.activemq.service;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQQueue;
import org.green.ipf.clean.activemq.main.CleanActiveMQMain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import java.util.Set;

@Service
public class ActiveMqService {
    private Logger logger = LoggerFactory.getLogger(CleanActiveMQMain.class);

    public void purgeAllQueue(String ip, String port) {
        logger.info("getting all queues");
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + ip + ":" + port);
            ActiveMQConnection connection = (ActiveMQConnection) connectionFactory.createConnection();
            connection.start();

            DestinationSource ds = connection.getDestinationSource();
            Set<ActiveMQQueue> queues = ds.getQueues();
            Thread.sleep(10000l); // this seems to ensure that all the queue is found
            logger.info("Queue count {}", queues != null ? queues.size() : 0);
            for (ActiveMQQueue activeMQQueue : queues) {
                try {
                    logger.info("queue: {}", activeMQQueue.getQueueName());
                    connection.destroyDestination(activeMQQueue);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
