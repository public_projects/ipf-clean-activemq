package org.green.ipf.clean.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfCleanActivemqApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpfCleanActivemqApplication.class, args);
	}

}
