package org.green.ipf.clean.activemq.main;

import org.green.ipf.clean.activemq.service.ActiveMqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CleanActiveMQMain implements CommandLineRunner {

    @Autowired
    private ActiveMqService activeMqService;

    @Override
    public void run(String... args) throws Exception {
        activeMqService.purgeAllQueue("localhost", "61616");
    }

}
